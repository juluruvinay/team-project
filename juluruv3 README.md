## Team Project ##

University of Dayton

Department of Computer Science

CPS 474/574 Software/Language-based Security

Instructor: Dr. Phu Phung

# Authentication Attacks and Defence of Attacks #

## Team10 – Members: ##
1.	Juluru vinay, juluruv3@udayton.edu
2.	Deekshith Ravula, ravulad1@udayton.edu

# Project Objective and Plan #

Objective of our project is to perform an attack and bypass passwords on known websites of which we have login authenitcation which 
are vulnerable and applications like linkedin, snapchat , facebook, yahoo etc.,with different attacks such as Brute force attack , 
Main in the Middle Attack , Phishing Attack , Credential Stuffing , Key Loggers. Sometimes a hacker knew the username or login id 
but they will be not aware of passwords so they will use different kinds of attacks to login.

# Project Managment #
So, Vinay juluru will perform these attacks and go throught that authentication and bypass it. And the Deekshith ravula will mention 
all the preventive measures and do necessary changes so that all these attacks cant be done easily and to make sure that 
authentication cant be bypassed with several kind of attacks

# Project Accomplishments #
Performing different type of attacks to penetrate through authentication either in website or Applications.

Performing defense by code or by tools or by preventive measures for the same to prevent attacks.

## Plan of Attack ##
Credential Stuffing :

By using OpenBullet Tool , a cyberattack method in which attackers use lists of compromised user credentials to breach into a system.
The attack uses bots for automation and scale and is based on the assumption that many users reuse usernames and passwords across
multiple services. 1. Use unique passwords for each service 2. Use a web application firewall 3. Limit authentication requests so 
that they cant apply bruteforce and penetrate easily by using saved passwords. 4.Use multi-factor authentication 5. Screen for 
leaked credentials , u can check your email address or mobile number in this HaveIBeenPwned.com website to see if ur credentials are 
leaked online. If leaked u have to change ur passwords for sure to defend a cyber attack.

## Plan of Defense ## Key Logger : 1. Use a firewall manager 2. Use anti key logger tools like ghostpress , kl-detector to make sure 
that key loggers wont capture ur data 3. Check if there is any keylogger in ur toolbar and remove it.

By using some of these kind of attacks we can perform an attack and by pass through the authentication.


